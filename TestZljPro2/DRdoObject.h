//
//  DRdoObject.h
//  TestZljPro1Demo
//
//  Created by rogue on 2019/1/4.
//  Copyright © 2019 rogue. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DRdoObject : NSObject

+ (NSString *)testDoName:(NSString *)name;

+ (NSString *)testGetName;

@end

NS_ASSUME_NONNULL_END
