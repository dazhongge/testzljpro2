//
//  DRdoObject.m
//  TestZljPro1Demo
//
//  Created by rogue on 2019/1/4.
//  Copyright © 2019 rogue. All rights reserved.
//

#import "DRdoObject.h"

@implementation DRdoObject

+ (NSString *)testDoName:(NSString *)name {
    NSString *doname = [NSString stringWithFormat:@"xianshi:%@", name];
    return doname;
}

+ (NSString *)testGetName {
    return @"testGetName";
}

@end
